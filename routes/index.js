const fs = require("fs");
const express = require('express');
const router = express.Router();
const appConstants = require("../constants/appConstants")

/* GET home page. */
router.get('/getjson', function (req, res, next) {
  res.status(200).json(appConstants.DATA);
});

router.get('/getfile', async function (req, res, next) {
  try {
    fs.writeFileSync("/tmp/file.json", JSON.stringify(appConstants.DATA));
  } catch (err) {
    res.status(400).send({ message: "Error writing file", err });
    return
  }

  res.status(200).download("/tmp/file.json");
});

module.exports = router;
