const DATA = [
    {
        "gender": "female",
        "name": {
            "title": "mademoiselle",
            "first": "simona",
            "last": "lacroix"
        },
        "location": {
            "street": "2846 rue du village",
            "city": "meltingen",
            "state": "schaffhausen",
            "postcode": 6882,
            "coordinates": {
                "latitude": "-50.6992",
                "longitude": "0.0583"
            },
            "timezone": {
                "offset": "-4:00",
                "description": "Atlantic Time (Canada), Caracas, La Paz"
            }
        },
        "email": "simona.lacroix@example.com",
        "login": {
            "uuid": "e7a44d89-b7ec-40db-bc1a-224223ad1b3a",
            "username": "purpledog229",
            "password": "beacon",
            "salt": "B6Xa1R9X",
            "md5": "0013f7364a4cbb6e41980974f3594567",
            "sha1": "5c29c373ac74a9e28868174dd30a21902ace0142",
            "sha256": "a8d8ecb09640c8aca946f3e3a24da27cc8e93bf424c192e0e749e2b59184e716"
        },
        "dob": {
            "date": "1958-10-21T09:51:31Z",
            "age": 60
        },
        "registered": {
            "date": "2005-02-27T08:34:46Z",
            "age": 14
        },
        "phone": "(817)-267-7261",
        "cell": "(212)-246-6066",
        "id": {
            "name": "AVS",
            "value": "756.4072.8333.63"
        },
        "picture": {
            "large": "https://randomuser.me/api/portraits/women/14.jpg",
            "medium": "https://randomuser.me/api/portraits/med/women/14.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/women/14.jpg"
        },
        "nat": "CH"
    },
    {
        "gender": "male",
        "name": {
            "title": "mr",
            "first": "côme",
            "last": "rousseau"
        },
        "location": {
            "street": "8100 rue courbet",
            "city": "nancy",
            "state": "lozère",
            "postcode": 21934,
            "coordinates": {
                "latitude": "52.7263",
                "longitude": "75.5320"
            },
            "timezone": {
                "offset": "-9:00",
                "description": "Alaska"
            }
        },
        "email": "côme.rousseau@example.com",
        "login": {
            "uuid": "3a242436-990a-4a70-b4d6-65a4852e1c78",
            "username": "tinymeercat336",
            "password": "googoo",
            "salt": "g2hpuaaO",
            "md5": "b90024c600a37a8efcc43d17d47e2210",
            "sha1": "e47f6f6368aa88ebe9747ecaaae3d33af21a459d",
            "sha256": "6484899d288ff6e522e2749a34c8f6ffd78902d41cdfe87686849f7040431f50"
        },
        "dob": {
            "date": "1996-04-11T13:14:07Z",
            "age": 23
        },
        "registered": {
            "date": "2008-10-25T09:27:00Z",
            "age": 10
        },
        "phone": "01-27-25-65-94",
        "cell": "06-09-77-64-20",
        "id": {
            "name": "INSEE",
            "value": "1NNaN05514244 94"
        },
        "picture": {
            "large": "https://randomuser.me/api/portraits/men/69.jpg",
            "medium": "https://randomuser.me/api/portraits/med/men/69.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/men/69.jpg"
        },
        "nat": "FR"
    },
    {
        "gender": "male",
        "name": {
            "title": "mr",
            "first": "kadir",
            "last": "akman"
        },
        "location": {
            "street": "2331 bağdat cd",
            "city": "çorum",
            "state": "kayseri",
            "postcode": 97411,
            "coordinates": {
                "latitude": "20.2193",
                "longitude": "-132.2605"
            },
            "timezone": {
                "offset": "-8:00",
                "description": "Pacific Time (US & Canada)"
            }
        },
        "email": "kadir.akman@example.com",
        "login": {
            "uuid": "1d23500a-6e94-4f6b-a3c4-8a9f70fcba74",
            "username": "orangeswan871",
            "password": "pooper",
            "salt": "frhu8uQE",
            "md5": "d682f6bfff13df73673ff5baf4f5e9ff",
            "sha1": "c6e34061a1ed83cbf21f145079785c1fe3b32d24",
            "sha256": "07dde6e40c202cf82d07e17f4f82314ea22558b9351a9ed35bef916329e821c1"
        },
        "dob": {
            "date": "1980-08-12T17:36:18Z",
            "age": 38
        },
        "registered": {
            "date": "2008-09-09T19:37:34Z",
            "age": 10
        },
        "phone": "(641)-369-7235",
        "cell": "(021)-730-3156",
        "id": {
            "name": "",
            "value": null
        },
        "picture": {
            "large": "https://randomuser.me/api/portraits/men/23.jpg",
            "medium": "https://randomuser.me/api/portraits/med/men/23.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/men/23.jpg"
        },
        "nat": "TR"
    },
    {
        "gender": "female",
        "name": {
            "title": "ms",
            "first": "aldalgisa",
            "last": "alves"
        },
        "location": {
            "street": "9717 rua são paulo ",
            "city": "camaragibe",
            "state": "amapá",
            "postcode": 39115,
            "coordinates": {
                "latitude": "-83.1069",
                "longitude": "-165.7716"
            },
            "timezone": {
                "offset": "-7:00",
                "description": "Mountain Time (US & Canada)"
            }
        },
        "email": "aldalgisa.alves@example.com",
        "login": {
            "uuid": "8826367e-9e07-4123-937f-1415f5a7b8dd",
            "username": "crazytiger743",
            "password": "world",
            "salt": "NU9OGbDN",
            "md5": "9e9386758f1077650af38aa94c18269d",
            "sha1": "007a2c302cdbef359da57d71324a9b68a5c9746f",
            "sha256": "49011413449eb3e2018a6ef0961656d66b606591b43a122b21e41591e888af70"
        },
        "dob": {
            "date": "1997-04-05T18:55:02Z",
            "age": 22
        },
        "registered": {
            "date": "2014-10-16T03:03:11Z",
            "age": 4
        },
        "phone": "(05) 2672-7327",
        "cell": "(26) 8322-6272",
        "id": {
            "name": "",
            "value": null
        },
        "picture": {
            "large": "https://randomuser.me/api/portraits/women/35.jpg",
            "medium": "https://randomuser.me/api/portraits/med/women/35.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/women/35.jpg"
        },
        "nat": "BR"
    },
    {
        "gender": "female",
        "name": {
            "title": "ms",
            "first": "ellen",
            "last": "craig"
        },
        "location": {
            "street": "8075 edwards rd",
            "city": "bathurst",
            "state": "queensland",
            "postcode": 6356,
            "coordinates": {
                "latitude": "32.5508",
                "longitude": "-36.5021"
            },
            "timezone": {
                "offset": "-11:00",
                "description": "Midway Island, Samoa"
            }
        },
        "email": "ellen.craig@example.com",
        "login": {
            "uuid": "3a6259fb-4680-432c-9062-8a1c00f2fc21",
            "username": "goldenkoala834",
            "password": "lighter",
            "salt": "lw5xGRnA",
            "md5": "6ac3944ff955c8b4b84c6252faed746a",
            "sha1": "6bcc19d0661acff04b31f45c32378af2de3f0411",
            "sha256": "f7a29e74242f69e301976de98746fda9b9d541db8dda5af19bfc6e4080277d5b"
        },
        "dob": {
            "date": "1962-08-13T12:11:37Z",
            "age": 56
        },
        "registered": {
            "date": "2016-04-18T23:31:08Z",
            "age": 3
        },
        "phone": "00-5367-2874",
        "cell": "0498-330-072",
        "id": {
            "name": "TFN",
            "value": "050052518"
        },
        "picture": {
            "large": "https://randomuser.me/api/portraits/women/6.jpg",
            "medium": "https://randomuser.me/api/portraits/med/women/6.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/women/6.jpg"
        },
        "nat": "AU"
    }
]



module.exports = {
    DATA
}