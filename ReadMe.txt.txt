Created API is using express.js. Because it is a simple APIs, to simplify the deployment, All endpoints are deployed in 1 API gateway. 
With that said, if there's any change to the API, we just need to update the code and don't have to change the AWS configuration.

-- Deploying the API --
1. Zip required these files":
	1.1 constants (folder)
	1.2 node_modules (folder)
	1.3 routes (folder)
	1.4 app.js
	1.5 lambda.js
2. Create and Login to console.aws.amazon.com
3. go to console.aws.amazon.com/lambda/home?region=us-east-2#/functions
4. Create function named "octagon-studio" with runtime environment "Node.js 10.x"
5. In "Function code" section, change "Handler" filed to from "index.handler" to "lambda.handler"
6. In "Function code" section, select the "code entry type" to "Upload a .zip file".
7. Click upload and select the zip file from step 1.
8. Click save button at the top right corner.
9. In "Designer" section, click "Add trigger" button.
10. Select "Api Gateway", Select security as "Open", and click "Add".
11. Select resources on the explorer tab.
12. In Resources explorer tab, Select root "/", Click "Actions" dropdown and select "Create resource"
13. Check the "Configure as proxy resource" and click "Create Resource"
14. Setup Method on the recently created "/{proxy+}" by clicking "Set up now".
15. At the "Lambda Function" field, tipe "octagon-studio" and click save.
16. Now your API is ready.

-- Check API -- 
1. Go to console.aws.amazon.com/lambda/home?region=us-east-2#/functions
2. In "Designer" section, click API Gateway.
3. At the "API Gateway" section, you will see the list of API with something like this:
	https://o5tijuk7jc.execute-api.us-east-2.amazonaws.com/default/{proxy+}
4. Change the "{proxy+}" with "getjson" or "getfile"
	https://o5tijuk7jc.execute-api.us-east-2.amazonaws.com/default/getjson
	https://o5tijuk7jc.execute-api.us-east-2.amazonaws.com/default/getfile

-- Secure API by adding API key token --
1. Open "Resources" on the explorer tab.
2. In Resources tab, Select "any" under "/{proxy+}
3. Click "Method Request" on the right panel
4. Change "API Key Required" from false to true
5. Open "API Keys" on the explorer tab.
6. Click "Actions" dropdown and select "Create API key", put "octagon-key" in the name field, click save.
7. Open "Usage Plans" on the explorer tab.
8. Click create, put "octagon-plan" in the name field, put 100 in "Rate", put 200 in "Burst", put 5000 in quota. click next.
9. Click "Add API stage", select "octagon-studio-API", select "default for the stage, click check button and next.
10. Click "Add API Key to Usage Plan", type "octagon-key" and select the suggested option. Click check button and done.
11. Open "Resources" on the explorer tab, click "Actions" dropdown and select "Deploy API"
12. Now your API won't work without the key. Find the key on "API Keys" on the explorer tab, select "octagon-key", and click show beside "API key".
13. To use it you need to send request using headers with the key. Something like:
	"x-api-key: 0IsGUwUoxA6LPaHb76CPT3xHcEgAsOnn1ig3hgLO"

-- Add CORS for domain whitelist --
1. Open "Resources" on the explorer tab.
2. Select "/{proxy+}.
3. Click "Actions" dropdown and select "Enable CORS".
4. Click "Enable CORS and replace existing CORS headers" and follow the instrucion.
5. Open "Resources" on the explorer tab, click "Actions" dropdown and select "Deploy API".
6. Now your API is available to be used from other client domains. For test purpose, all domains are allowed.
